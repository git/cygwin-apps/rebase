/*
 * PE32 checksum
 *
 * Copyright (C) 2023 Christian Franke
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef PE_CHECKSUM_H
#define PE_CHECKSUM_H

/*
 * Calculate PE32 checksum of an executable file and optionally fix it.
 *
 * fd: File descriptor of file opened R/O (!set) or R/W (set) in binary mode.
 * set: If nonzero, the correct checksum is written to the file if required.
 * old_checksum: If non-NULL, the previous checksum is returned here.
 *
 * return value: The calculated checksum or 0 on error (the checksum itself
 * is never 0).  On error, errno is preserved from failed I/O or set to EINVAL
 * if the file format is invalid.
 */
unsigned pe32_checksum(int fd, int set, unsigned * old_checksum);

#endif
