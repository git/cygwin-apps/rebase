/*
 * PE32 checksum
 *
 * Copyright (C) 2023 Christian Franke
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "pechecksum.h"

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

static inline unsigned get_word(const unsigned char * p)
{
  return p[0] | (p[1] << 8);
}

static inline unsigned get_dword(const unsigned char * p)
{
  return p[0] | (p[1] << 8) | (p[2] << 16) | (p[3] << 24);
}

static inline void put_dword(unsigned char * p, unsigned val)
{
  p[0] = (unsigned char) val;
  p[1] = (unsigned char)(val >>  8);
  p[2] = (unsigned char)(val >> 16);
  p[3] = (unsigned char)(val >> 24);
}

unsigned pe32_checksum(int fd, int set, unsigned * old_checksum)
{
  // Read headers.
  const unsigned bufsiz = 4096;
  unsigned char buf[bufsiz];
  if (lseek(fd, 0, SEEK_SET) != 0)
    return 0;
  int nr = read(fd, buf, bufsiz);
  if (nr < 0)
    return 0;
  if (nr <= 0x200) {
    errno = EINVAL;
    return 0;
  }

  // IMAGE_DOS_HEADER.pe_magic == "MZ" ?
  if (get_word(buf) != 0x5a4d) {
    errno = EINVAL;
    return 0;
  }
  // pehdr_offs = IMAGE_DOS_HEADER.lfa_new
  unsigned pehdr_offs = get_dword(buf + 0x3c);
  if (!(0x40 <= pehdr_offs && pehdr_offs <= nr - 0x100)) {
    errno = EINVAL;
    return 0;
  }
  // IMAGE_NT_HEADERS.Signature == "PE" ?
  if (get_word(buf + pehdr_offs) != 0x4550) {
    errno = EINVAL;
    return 0;
  }
  // old_sum = IMAGE_OPTIONAL_HEADER(32|64).CheckSum
  unsigned sum_offs = pehdr_offs + 0x58;
  unsigned old_sum = get_dword(buf + sum_offs);
  if (old_checksum)
    *old_checksum = old_sum;

  // Clear old checksum because it is included below.
  put_dword(buf + sum_offs, 0);

  // Calc new checksum.
  unsigned sum = 0, size = 0;
  int i = 0;
  for (;;) {
    sum += get_word(buf + i);
    sum = (sum + (sum >> 16)) & 0xffff;

    if ((size += 2) >= 0x40000000) {
      // 1GiB, assume something is wrong.
      errno = EINVAL;
      return 0;
    }
    if ((i += 2) < nr - 1)
      continue; // ... with next 2 bytes.

    // Assume that there are no short reads.
    if (i < nr)
      break; // Last byte.
    i = 0;
    if ((nr = read(fd, buf, bufsiz)) < 0)
      return 0;
    if (nr < 2)
      break; // Last byte or EOF.
    // Continue with next block.
  }

  // Handle last byte of file with uneven size.
  if (i < nr) {
    sum += buf[i];
    sum = (sum + (sum >> 16)) & 0xffff;
    size++;
  }

  // Add filesize to use some of the upper 16 bits.
  sum += size;

  // Fix the checksum if requested and required.
  if (set && old_sum != sum) {
    put_dword(buf, sum);
    if (lseek(fd, sum_offs, SEEK_SET) == -1)
      return 0;
    if (write(fd, buf, 4) == -1)
      return 0;
  }

  return sum;
}

#if STANDALONE
//
// Test program
//
#include <stdio.h>
#include <string.h>

// Optionally check result using native imagehlp.dll function.
#if STANDALONE > 1
#include <windows.h>
#include <imagehlp.h>
#endif

int main(int argc, char ** argv)
{
  int i = 1, set = 0;
  if (i < argc && !strcmp(argv[i], "-s")) {
    set = 1;
    i++;
  }
  if (i >= argc) {
    printf("Usage: %s [-s] FILE...\n", argv[0]);
    return 1;
  }

  int status = 0;
  for ( ; i < argc; i++) {
    const char * name = argv[i];

    int fd = open(name, (set ? O_RDWR : O_RDONLY) | O_BINARY);
    if (fd == -1) {
      perror(name);
      status |= 0x1;
      continue;
    }

    unsigned osum = 0;
    unsigned sum = pe32_checksum(fd, set, &osum);
    int err = errno;
    close(fd);

    if (sum == 0) {
      fprintf(stderr, "%s: Failed, errno=%d\n", name, err);
      status |= 0x1;
      continue;
    }
    if (osum == sum) {
      printf("%s: 0x%08x (OK)\n", name, osum);
    }
    else {
      printf("%s: 0x%08x -> 0x%08x (%s)\n", name, osum, sum,
             (set ? "Fixed" : "*Not fixed*"));
      if (!set)
        status |= 0x2;
    }

#if STANDALONE > 1
    DWORD osum2 = 0, sum2 = 0;
    DWORD rc = MapFileAndCheckSumA(name, &osum2, &sum2);
    if (rc != CHECKSUM_SUCCESS) {
      fprintf(stderr, "%s: MapFileAndCheckSumA() failed with error %u\n", name, rc);
      status |= 0x1;
    }
    else if (!(osum2 == (set ? sum : osum) && sum2 == sum)) {
      fprintf(stderr, "%s: MapFileAndCheckSumA() results differ: 0x%08x -> 0x%08x\n",
              name, osum2, sum2);
      status |= 0x4;
    }
#endif
  }

  return status;
}

#endif // STANDALONE
