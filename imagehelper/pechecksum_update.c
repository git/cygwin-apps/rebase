/*
 * PE32 checksum update
 *
 * Copyright (C) 2023 Christian Franke
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "pechecksum_update.h"

uint16_t pe32_checksum_update_begin(uint32_t checksum, unsigned long long filesize)
{
  if (!(0 < checksum && filesize < checksum && checksum - filesize <= 0xffffULL))
    return 0;
  return (uint16_t)(checksum - filesize);
}

static inline uint16_t get_word(const void * p, unsigned i)
{
  const uint8_t * bp = p;
  return bp[i] | (bp[i + 1] << 8);
}

uint16_t pe32_checksum_update_add(uint16_t checksum16, const void * old_block,
                                  const void * new_block, unsigned size)
{
  if (!(checksum16 && !((uintptr_t)old_block & 1)
        && !((uintptr_t)new_block & 1) && !(size & 1)))
    return 0;
  uint32_t sum = checksum16;

  for (unsigned i = 0; i < size; i += 2) {
    uint16_t old_word = get_word(old_block, i);
    uint16_t new_word = get_word(new_block, i);
    if (new_word == old_word)
      continue;
    // Note: This does not work for updates of the first nonzero word of the file.
    // This does not matter because the first word "MZ" is never changed.
    if (old_word < sum) // Never return to 0, so no '<='.
      sum -= old_word;
    else // Revert previous 16-bit overflow, skip 0.
      sum += 0xffffU - old_word;
    sum += new_word;
    // If 16-bit overflow, skip 0.
    sum = (sum + (sum >> 16)) & 0xffffU;
  }

  return (uint16_t)sum;
}

uint32_t pe32_checksum_update_end(uint16_t checksum16, unsigned long long filesize)
{
  if (!(checksum16 && checksum16 + filesize < 0xffffffffULL))
    return 0;
  return (uint32_t)(checksum16 + filesize);
}
