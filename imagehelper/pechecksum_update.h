/*
 * PE32 checksum update
 *
 * Copyright (C) 2023 Christian Franke
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef PE_CHECKSUM_UPDATE_H
#define PE_CHECKSUM_UPDATE_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Begin checksum update sequence.
 * checksum: Old 32-bit checksum from PE header.
 * filesize: Size of PE file.
 * return value: 16-bit checksum for p32_checksum_update_add()
 * or 0 on error (the checksum itself is never 0).
 */
uint16_t pe32_checksum_update_begin(uint32_t checksum, unsigned long long filesize);

/*
 * Update checksum.
 * checksum16: Current 16-bit checksum from p32_checksum_update_begin()
 * or this function.
 * old_block: Address of old (original) data block, must be word aligned.
 * new_block: Address of new (changed) data block, must be word aligned.
 * size: Number of bytes of both data blocks, must be even.
 * The PE checksum field may be included if identical in both blocks.
 * return value: Updated 16-bit checksum or 0 on error.
 */
uint16_t pe32_checksum_update_add(uint16_t checksum16, const void * old_block,
                                  const void * new_block, unsigned size);

/*
 * End checksum update sequence.
 * checksum16: New 16-bit checksum from pe32_checksum_update_add().
 * filesize: Size of PE file.
 * return value: New 32-bit checksum for PE header or 0 on error.
 */
uint32_t pe32_checksum_update_end(uint16_t checksum16, unsigned long long filesize);

#ifdef __cplusplus
}
#endif

#endif /* PE_CHECKSUM_UPDATE_H */
